﻿using FirstMVCAppEx.Models;
using System.Collections.Generic;

namespace FirstMVCAppEx.BL
{
    public static class KullaniciIslem
    {
        private static List<Kullanici> _kullanicilar = new List<Kullanici>();
        private static int _id = 0;
        public static void KullaniciEkle(Kullanici kullanici)
        {
            _id += 1;
            kullanici.Id = _id;
            _kullanicilar.Add(kullanici);
        }
        public static void KullaniciSil(int? Id)
        {
            Kullanici k = _kullanicilar.Find(x => x.Id == Id);
            if (k != null)
                _kullanicilar.Remove(k);
        }
        public static Kullanici KullaniciDetay(int Id)
        {
            return _kullanicilar.Find(x => x.Id == Id);
        }

        public static void KullaniciDuzenle(Kullanici kullanici)
        {
            Kullanici k = _kullanicilar.Find(x => x.Id == kullanici.Id);
            int index = _kullanicilar.FindIndex(x => x == k);
            _kullanicilar.RemoveAt(index);
            k = kullanici;
            _kullanicilar.Insert(index, k);

        }

        public static IEnumerable<Kullanici> getKullanicilar()
        {
            return _kullanicilar;
        }
    }
}