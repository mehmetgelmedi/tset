﻿using FirstMVCAppEx.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FirstMVCAppEx.ViewModel
{
    public class VM
    {
        public Kullanici kullanici { get; set; }
        public IEnumerable<Kullanici> kullanicilar { get; set; }
        public IList<SelectListItem> items = new List<SelectListItem>();

        public VM()
        {
            DereceDoldur();
        }

        private void DereceDoldur()
        {
            items.Add(new SelectListItem() { Text = "Ön Lisans", Value = "OnLisans", Selected = false });
            items.Add(new SelectListItem() { Text = "Lisans", Value = "Lisans", Selected = true });
            items.Add(new SelectListItem() { Text = "Yüksek Lisans", Value = "YuksekLisans", Selected = false });
            items.Add(new SelectListItem() { Text = "Doktora", Value = "Doktora", Selected = false });
        }
    }
}