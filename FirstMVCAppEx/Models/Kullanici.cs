﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FirstMVCAppEx.Models
{
    public class Kullanici
    {
        public int Id { get; set; }
        public string Ad { get; set; } //text
        public string Derece { get; set; } //ddl
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime DogumTarihi { get; set; } //date
        public bool Durum { get; set; } //check
        public string Sistem { get; set; } //radio
        public int Sayac { get; set; } //num
    }
}