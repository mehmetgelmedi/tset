﻿using FirstMVCAppEx.BL;
using FirstMVCAppEx.Models;
using FirstMVCAppEx.ViewModel;
using System;
using System.Web.Mvc;

namespace FirstMVCAppEx.Controllers
{
    public class HomeController : Controller
    {
        private VM vm = new VM();
        public ActionResult Index()
        {
            //KullaniciIslem.KullaniciEkle(new Kullanici { Ad = "m", Derece = "Lisans", DogumTarihi = DateTime.Now.Date, Sistem="Linux" ,Durum = true, Sayac = 5 });
            return View(VMGuncelle());
        }
        
        public ActionResult KullaniciEkle(Kullanici kullanici)
        {
            if (ModelState.IsValid)
                KullaniciIslem.KullaniciEkle(kullanici);
            return RedirectToAction("Index", "Home");
        }
        public ActionResult KullaniciSil(int? Id)
        {
            if(Id!=null)
                KullaniciIslem.KullaniciSil(Id);

            return RedirectToAction("Index", "Home");
        }
        public ActionResult KullaniciDetay(int Id)
        {
            vm.kullanici = KullaniciIslem.KullaniciDetay(Id);
            return PartialView("_Duzenle", vm);
        }
        public ActionResult KullaniciDuzenle(Kullanici kullanici)
        {
            KullaniciIslem.KullaniciDuzenle(kullanici);
            return RedirectToAction("Index", "Home");
        }
        public VM VMGuncelle()
        {
            vm.kullanicilar = KullaniciIslem.getKullanicilar();
            return vm;
        }
        public JsonResult getKullanicilar()
        {
            var jsonData = new
            {
                data = KullaniciIslem.getKullanicilar()
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
    }
}