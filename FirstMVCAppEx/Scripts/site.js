﻿function kullaniciEkleGoster() {
    $('#yeniEkleModal').modal();
}

var itemId;

function kullaniciDetayGoster() {
    if (itemId !== 0) {
        var data = {
            Id: itemId
        };
        $.post("/Home/KullaniciDetay",
            data,
            function (res) {
                $("#icerik").html(res);
                $("#detay").modal();
            });
    }
}
function kullaniciSilGoster() {
    if (itemId !== 0) {
        $('#sil-onayla').modal();
    }
}

$(document).ready(function () {
    itemId = 0;
    $('.btn-ok').click(function () {
        var data = {
            Id: itemId
        };
        $.post("/Home/KullaniciSil",
            data,
            function (res) {
                location.reload();
            });
    });

    var dataTableKullanicilar = $('#dataTableKullanicilar').DataTable({
        "searching": true,
        "ordering": true,
        "responsive": true,
        "lengthChange": false,
        "pagingType": "full_numbers",
        "ajax": "/Home/getKullanicilar",
        "columns": [
            { "data": "Id" },
            { "data": "Ad" },
            { "data": "Derece" },
            { "data": "DogumTarihi" },
            { "data": "Durum" },
            { "data": "Sistem" },
            { "data": "Sayac" }
        ],
    });

    $('#dataTableKullanicilar tbody').on('click',
        'tr',
        function () {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
                itemId = 0;
            }
            else {
                dataTableKullanicilar.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
                itemId = $(this).find('.sorting_1').html();
            }
        });
});